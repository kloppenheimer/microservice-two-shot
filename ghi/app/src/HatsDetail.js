import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap'


function HatColumn(props) {
const [show, setShow] = useState(false);
const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

const deleteHats = async(href) => {
    const url = `http://localhost:8090${href}`;
    console.log(href)
    const fetchConfig = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    };
    await fetch(url, fetchConfig)
    props.fetchData()
    handleClose()

}
  return (
    <div className="col">
      {props.list.map(data => { 
        return (
          <div key={data.href} className="card mb-3 shadow">
            <img src={data.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{data.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                <div>Closet: {data.location.closet_name}</div>
              </h6>
              <ul className="card-text">
                <li>{data.color}</li>
                <li>{data.fabric}</li>
              </ul>
            </div>
            <div className="card-footer">
                <div className="d-grid">
                    <Button onClick={handleShow} variant="outline-danger" size="lg"  >
                        Delete
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                        <Modal.Body>Are You Sure?</Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                            <Button variant="danger" id={data.id} onClick={() => deleteHats(data.href)}>
                                Delete
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const HatsDetail = (props) =>  {
  const [hatColumns, sethatColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090${hat.href}`;
          requests.push(fetch(detailUrl));
          
        }

        const responses = await Promise.all(requests);

        const columns = [[], [], []];

        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        sethatColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);
  const hat_url = "https://media.istockphoto.com/id/154907228/photo/stack-of-baseball-caps-in-various-colors.jpg?s=612x612&w=0&k=20&c=5Ov4AEytFYSymQikyr2m637I3ZLPjSoi4x-p4zuN5gA="
  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src={hat_url} alt="" width="250" />
        <h1 className="display-5 fw-bold">HATS!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
          The ultimate tool for tracking and organizing an extensive collection of hats for your closet!
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          {hatColumns.map((hatList, index) => {
            return (
              <HatColumn key={index} list={hatList} fetchData={fetchData} />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default HatsDetail
