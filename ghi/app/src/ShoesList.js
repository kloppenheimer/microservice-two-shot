import React, {useEffect, useState } from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                return (
                    <tr key={shoe.href}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                    </tr>
                );
            })}
            </tbody>
            </table>
        </>
    )
}

export default ShoesList;
