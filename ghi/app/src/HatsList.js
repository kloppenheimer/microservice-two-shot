import { useEffect, useState } from 'react'

function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const url = 'http://localhost:8090/api/hats/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setHats(data.hats)
            
        }
    }
    
    useEffect(()=>{
        getData()
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style Name</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.location }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default HatsList
