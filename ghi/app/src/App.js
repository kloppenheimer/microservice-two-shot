import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import HatsForm from './HatsForm';
import HatsDetail from './HatsDetail';

function App() {

  return (
    <>
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route element={<ShoesList />} />
            <Route path="new" element={<ShoesForm />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatsDetail />} />
            <Route path="new" element={<HatsForm />} />
          </Route>
        </Routes>
    </BrowserRouter>
    </>
  );
}

export default App;
