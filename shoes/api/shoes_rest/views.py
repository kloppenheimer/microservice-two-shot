from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import BinVO, Shoes
import json


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
        ]

class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = ["model_name", "id"]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            print("Content:", bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def show_shoes(request, pk):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            False,
        )

    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)

        try:
            if "bin" in content:
                bin = BinVO.objects.get(bin=content["bin"])
                content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin location"},
                status=400,
            )

        Shoes.objects.filter(id=pk).update(**content)
        shoe = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            False
        )
