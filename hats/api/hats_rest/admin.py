from django.contrib import admin

# Register your models here.


# This is a relative import of user from the models
# module in the same directory ".models"

from hats_rest.models import LocationVO

admin.site.register(LocationVO)
