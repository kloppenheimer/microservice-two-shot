# Generated by Django 4.0.3 on 2023-12-14 20:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0006_locationvo_section_number'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='locationvo',
            name='section_number',
        ),
    ]
