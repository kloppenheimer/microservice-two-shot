# Generated by Django 4.0.3 on 2023-12-14 20:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0005_rename_url_picture_hat_picture_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='section_number',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
