# Generated by Django 4.0.3 on 2023-12-14 06:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_hat_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='url_picture',
            field=models.URLField(null=True),
        ),
    ]
